/*
** epikong.h for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 15:30:12 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:28:25 2014 Thomas de Beauchene
*/

#ifndef		EPIKONG_H_
# define	EPIKONG_H_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <stdlib.h>
#include <stdio.h>

# define BLOCK_SIZE		32

# define NB_BLOCKS_HEIGHT	16
# define NB_BLOCKS_WIDTH	32

# define WIN_HEIGHT		NB_BLOCKS_HEIGHT * BLOCK_SIZE
# define WIN_WIDTH		NB_BLOCKS_WIDTH * BLOCK_SIZE

# define SUCCESS		0
# define FAILURE		-1

typedef enum	e_direction
  {
    LEFT,
    RIGHT,
    FRONT,
    BACK
  }		t_direction;

typedef enum	e_type
  {
    TYPE_EMPTY,
    TYPE_WALL,
    TYPE_SCALE,
    TYPE_KEY,
    TYPE_MONSTER,
    TYPE_ENTRY,
    TYPE_EXIT,
    TYPE_LAST
  }		t_type;

typedef enum	e_sprite
  {
    SPRITE_WALL,
    SPRITE_SCALE,
    SPRITE_KEY,
    SPRITE_MONSTER_LEFT,
    SPRITE_MONSTER_RIGHT,
    SPRITE_HERO_FRONT,
    SPRITE_HERO_BACK,
    SPRITE_HERO_LEFT,
    SPRITE_HERO_RIGHT,
    SPRITE_EXIT,
    SPRITE_LAST
  }		t_sprite;

typedef struct	s_hero
{
  int		life;
  int		weapon;
  SDL_Rect	pos;
  t_direction	direction;
  int		current_sprite;
  int		nb_sprites;
  SDL_Surface	*sprite_set[SPRITE_LAST];
}		t_hero;

typedef struct		s_monster
{
  SDL_Rect		pos;
  t_direction		direction;
  int			current_sprite;
  int			nb_sprites;
  SDL_Surface		*sprite_set[SPRITE_LAST];
  struct s_monster	*next;
}			t_monster;

typedef struct	s_game
{
  SDL_Surface	*screen;
  SDL_Surface	**sprites;
  char		*map;
  int		key_caught;
  SDL_Rect	key_pos;
  SDL_Rect	exit_pos;
  t_hero	hero;
  t_monster	*monsters;
}		t_game;

/*
** blit_elements.c
*/
void		blit_fixed_elements(SDL_Surface *screen,
				    char *map,
				    SDL_Surface **sprites);
void		blit_mobile_elements();
void		blit_monsters(SDL_Surface *screen, t_monster *monster);

/*
** check.c
*/
void		check_in_out(ssize_t, char*, size_t, FILE*);
void		check_wall_under_monster(char*, int);
int		check_monster(char*);
void		check_wall(char*);

/*
** event.c
*/
void		sdl_init();
void		sdl_event();

/*
** get_map.c
*/
void		map_is_valid(ssize_t read, char *line, size_t len, FILE *fp);
void		check_map(char*);
size_t		get_map_size(char*);
char		*get_map(char*);

/*
** main.c
*/
void		blit_level(SDL_Surface *screen,
			   char *map,
			   SDL_Surface **sprites);
void		blit_monsters(SDL_Surface *screen, t_monster *first);
void		blit_mobile_elements(t_game *game);

/*
** init.c
*/
void		init_window(SDL_Surface **screen);
void		init_hero(t_game *game, t_hero *hero);
void		init_monster(t_game *game, t_monster *monster);
void		init_game(t_game *game, int level_num);

/*
** load_level.c
*/
void		load_level(t_game *game, int level_nb);

/*
** menu.c
*/
int		choose_level();

/*
** my_exception.c
*/
void		my_exception(char*);

/*
** sprites.c
*/
SDL_Surface	**load_sprites();
void		release_ressources(t_game *game);

#endif /* !EPIKONG_H_ */
