/*
** event.c for epikong in /home/law-du_c/rush-epikong/src/part1
**
** Made by cedric law-dune
** Login   <law-du_c@epitech.net>
**
** Started on  Sat Mar  8 14:00:02 2014 cedric law-dune
** Last update Sun Mar  9 11:33:46 2014 Thomas de Beauchene
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "../include/epikong.h"

void		sdl_event()
{
  SDL_Event	event;
  int		i;

  i = 1;
  while (i)
    {
      SDL_WaitEvent(&event);
      if(event.type == SDL_QUIT)
	{
	  i = 0;
	  break;
	}
    }
}
