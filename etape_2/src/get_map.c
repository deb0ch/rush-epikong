/*
** get_map.c for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 15:28:13 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:27:30 2014 Thomas de Beauchene
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "epikong.h"

void		map_is_valid(ssize_t read, char *line, size_t len, FILE *fp)
{
  int		monster_pos;
  int		x;
  int		tmp;

  monster_pos = -1;
  x = 0;
  tmp = 0;
  while ((read = getline(&line, &len, fp)) != -1)
    {
      if (tmp == 0)
	tmp = read;
      if (tmp != read)
	my_exception("Invalid map: lines of different sizes");
      if (x == 0)
	check_wall(line);
      if (line[0] != 'w' || line[read - 2] != 'w')
	my_exception("Invalid map: must be surrounded by walls");
      if (monster_pos != -1)
	check_wall_under_monster(line, monster_pos);
      monster_pos = check_monster(line);
      x++;
    }
  check_wall(line);
  free(line);
}

void		check_map(char *file)
{
  FILE		*fp;
  char		*line;
  size_t	len;
  ssize_t	read;

  len = 0;
  line = NULL;
  read = 0;
  fp = fopen(file, "r");
  if (fp == NULL)
    my_exception("Could not open input file");
  map_is_valid(read, line, len, fp);
  fclose(fp);
  fp = fopen(file, "r");
  if (fp == NULL)
    my_exception("Could not open input file");
  check_in_out(read, line, len, fp);
  fclose(fp);
}

size_t		get_map_size(char *file)
{
  FILE		*fp;
  char		*line;
  size_t	len;
  ssize_t	read;
  int		x;
  int		y;

  y = 0;
  len = 0;
  line = NULL;
  fp = fopen(file, "r");
  if (fp == NULL)
    my_exception("Could not open input file");
  while ((read = getline(&line, &len, fp)) != -1)
    {
      x = read;
      y++;
    }
  if (line)
    free(line);
  fclose(fp);
  if (x != 33 || y != 16)
    my_exception("Map size is not valid");
  return (x * y);
}

char		*get_map(char *file)
{
  FILE		*f;
  int		c;
  int		i;
  char		*newmap;
  int		mapsize;

  i = 0;
  mapsize = get_map_size(file);
  f = fopen(file, "r");
  if (f == NULL)
    my_exception("Could not open input file");
  check_map(file);
  newmap = malloc(sizeof(char*) * mapsize);
  while ((c = fgetc(f)) != -1)
    {
      if (c != '\n')
	{
	  newmap[i] = c;
	  i += 1;
	}
    }
  newmap[i] = '\0';
  fclose(f);
  return (newmap);
}
