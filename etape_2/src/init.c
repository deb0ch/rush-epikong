/*
** init.c for epikong in /home/chauvo_t/rendu/rush-epikong/etape_2
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sun Mar  9 11:04:49 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:26:59 2014 Thomas de Beauchene
*/

#include "../include/epikong.h"

void	init_hero(t_game *game, t_hero *hero)
{
  hero->life = 3;
  hero->weapon = 0;
  hero->nb_sprites = 4;
  hero->sprite_set[LEFT] = game->sprites[SPRITE_HERO_LEFT];
  hero->sprite_set[RIGHT] = game->sprites[SPRITE_HERO_RIGHT];
  hero->sprite_set[BACK] = game->sprites[SPRITE_HERO_BACK];
  hero->sprite_set[FRONT] = game->sprites[SPRITE_HERO_FRONT];
  hero->direction = FRONT;
}

void	init_monster(t_game *game, t_monster *monster)
{
  monster->nb_sprites = 2;
  monster->sprite_set[LEFT] = game->sprites[SPRITE_MONSTER_LEFT];
  monster->sprite_set[RIGHT] = game->sprites[SPRITE_MONSTER_RIGHT];
  monster->direction = LEFT;
}

void	init_game(t_game *game, int level_num)
{
  memset(game, 0, sizeof(t_game));
  init_window(&game->screen);
  game->sprites = load_sprites();
  game->hero.current_sprite = SPRITE_HERO_FRONT;
  load_level(game, level_num);
  init_hero(game, &game->hero);
  game->monsters = NULL;
}

void	init_window(SDL_Surface **screen)
{
  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
  if ((*screen = SDL_SetVideoMode(WIN_WIDTH,
				  WIN_HEIGHT,
				  32,
				  SDL_HWSURFACE)) == NULL)
    my_exception("Could not create window");
  SDL_WM_SetCaption("EpiKong", NULL);
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
}
