/*
** choose_level.c for epikong in /home/chauvo_t/rendu/rush-epikong/etape_2
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sun Mar  9 10:40:33 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:27:54 2014 Thomas de Beauchene
*/

#include "../include/epikong.h"

void		load_monster(t_game *game, SDL_Rect *pos)
{
  t_monster	*new;

  if ((new = malloc(sizeof(t_monster))) == NULL)
    my_exception("Malloc fail");
  new->pos = *pos;
  new->next = game->monsters;
  game->monsters = new;
}

void		init_elements(t_game *game)
{
  SDL_Rect	pos;
  int		i;

  i = 0;
  while (i < NB_BLOCKS_WIDTH * NB_BLOCKS_HEIGHT)
    {
      pos.x = (i % NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      pos.y = (i / NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      if (game->map[i] == 'i')
	game->hero.pos = pos;
      if (game->map[i] == 'm')
	load_monster(game, &pos);
      if (game->map[i] == 'k')
	game->key_pos = pos;
      if (game->map[i] == 'o')
	game->exit_pos = pos;
      ++i;
    }
}

void		load_level(t_game *game, int level_num)
{
  static char	*map_files[] =
    {
      "levels/level_1.map",
      "levels/level_2.map",
      "levels/level_3.map",
      "levels/level_4.map",
      "levels/level_5.map"
    };

  if (level_num >= 1 && level_num <= 5)
    game->map = get_map(map_files[level_num - 1]);
  else
    game->map = get_map(map_files[0]);
  init_elements(game);
}
