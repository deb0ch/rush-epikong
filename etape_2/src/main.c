/*
** main.c for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 16:52:49 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:28:26 2014 Thomas de Beauchene
*/

#include "../include/epikong.h"

int		main()
{
  t_game	game_data;
  int		level_num;

  level_num = 1;
  init_game(&game_data, level_num);
  blit_fixed_elements(game_data.screen, game_data.map, game_data.sprites);
  blit_mobile_elements(&game_data);
  SDL_Flip(game_data.screen);
  sdl_event();
  release_ressources(&game_data);
  return (EXIT_SUCCESS);
}
