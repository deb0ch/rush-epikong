/*
** menu.c for epikong in /home/chauvo_t/rendu/rush-epikong/etape_2
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sun Mar  9 16:59:29 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:28:25 2014 Thomas de Beauchene
*/

#include "../include/epikong.h"

int		choose_level(SDL_Surface *screen)
{
  SDL_Surface	*background;
  SDL_Rect	pos;
  SDL_Event	event;

  pos.x = 0;
  pos.y = 0;
  if ((background = IMG_Load("../sprites/menu.png")) == NULL)
    my_exception("could not load menu background");
  SDL_BlitSurface(background, NULL, screen, &pos);
  SDL_Flip(screen);
  while (42)
    {
      SDL_WaitEvent(&event);
      if (event.type == SDL_QUIT)
	return (0);
      if (event.type == SDL_KEYDOWN
	  && (event.key.keysym.sym == SDLK_KP1
	      || event.key.keysym.sym == SDLK_1))
	  return (1);
      if (event.type == SDL_KEYDOWN
	  && (event.key.keysym.sym == SDLK_KP2
	      || event.key.keysym.sym == SDLK_2))
	  return (2);
      if (event.type == SDL_KEYDOWN
	  && (event.key.keysym.sym == SDLK_KP3
	      || event.key.keysym.sym == SDLK_3))
	  return (3);
      if (event.type == SDL_KEYDOWN
	  && (event.key.keysym.sym == SDLK_KP4
	      || event.key.keysym.sym == SDLK_4))
	  return (4);
      if (event.type == SDL_KEYDOWN
	  && (event.key.keysym.sym == SDLK_KP5
	      || event.key.keysym.sym == SDLK_5))
	  return (5);
    }
}
