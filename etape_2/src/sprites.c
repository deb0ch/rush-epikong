/*
** sprites.c for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 18:22:51 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:28:07 2014 Thomas de Beauchene
*/

#include "../include/epikong.h"

void		check_loading(SDL_Surface **sprites)
{
  int		i;

  i = 0;
  while (i < SPRITE_LAST)
    {
      if (sprites[i] == NULL)
	my_exception("could not load sprites");
      i += 1;
    }
}

SDL_Surface	**load_sprites()
{
  SDL_Surface	**sprites;

  if ((sprites = malloc(SPRITE_LAST * sizeof(SDL_Surface*))) == NULL)
    my_exception("could not allocate sprites array");
  sprites[SPRITE_WALL] = IMG_Load("../sprites/world/ground.png");
  sprites[SPRITE_SCALE] = IMG_Load("../sprites/world/scale.png");
  sprites[SPRITE_KEY] = IMG_Load("../sprites/key/key0.png");
  sprites[SPRITE_MONSTER_LEFT] = IMG_Load("../sprites/monster/move_right.png");
  sprites[SPRITE_MONSTER_RIGHT] = IMG_Load("../sprites/monster/move_left.png");
  sprites[SPRITE_HERO_FRONT] = IMG_Load("../sprites/rayman/front.png");
  sprites[SPRITE_HERO_BACK] = IMG_Load("../sprites/rayman/back.png");
  sprites[SPRITE_HERO_LEFT] = IMG_Load("../sprites/rayman/move_left.png");
  sprites[SPRITE_HERO_RIGHT] = IMG_Load("../sprites/rayman/move_right.png");
  sprites[SPRITE_EXIT] = IMG_Load("../sprites/world/door.png");
  check_loading(sprites);
  return (sprites);
}

void	release_ressources(t_game *game)
{
  int	i;

  i = 0;
  while (i < SPRITE_LAST - 1)
    {
      SDL_FreeSurface((game->sprites)[i]);
      i += 1;
    }
  free(game->sprites);
  game->sprites = NULL;
  SDL_Quit();
}
