/*
** blit_elements.c for epikong in /home/chauvo_t/rendu/rush-epikong/etape_2
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sun Mar  9 11:45:09 2014 Thomas de Beauchene
** Last update Sun Mar  9 19:27:43 2014 Thomas de Beauchene
*/

#include "../include/epikong.h"

void		blit_fixed_elements(SDL_Surface *screen,
				    char *map,
				    SDL_Surface **sprites)
{
  SDL_Rect	pos;
  int		i;

  i = 0;
  while (i < NB_BLOCKS_WIDTH * NB_BLOCKS_HEIGHT)
    {
      pos.x = (i % NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      pos.y = (i / NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      if (map[i] == 'w')
	SDL_BlitSurface(sprites[SPRITE_WALL], NULL, screen, &pos);
      else if (map[i] == 's')
	SDL_BlitSurface(sprites[SPRITE_SCALE], NULL, screen, &pos);
      else if (map[i] == 'o')
	SDL_BlitSurface(sprites[SPRITE_EXIT], NULL, screen, &pos);
      i += 1;
    }
}

void		blit_monsters(SDL_Surface *screen, t_monster *first)
{
  t_monster	*tmp;

  tmp = first;
  while (tmp != NULL)
    {
      SDL_BlitSurface(tmp->sprite_set[tmp->direction],
		      NULL, screen, &tmp->pos);
      tmp = tmp->next;
    }
}

void	blit_mobile_elements(t_game *game)
{
  blit_monsters(game->screen, game->monsters);
  if (!game->key_caught)
    SDL_BlitSurface(game->sprites[SPRITE_KEY],
		    NULL, game->screen, &game->key_pos);
  SDL_BlitSurface(game->sprites[game->hero.current_sprite],
		  NULL, game->screen, &game->hero.pos);
}
