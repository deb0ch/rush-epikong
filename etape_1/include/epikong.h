/*
** epikong.h for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 15:30:12 2014 Thomas de Beauchene
** Last update Sun Mar  9 18:20:50 2014 cedric law-dune
*/

#ifndef		EPIKONG_H_
# define	EPIKONG_H_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <stdlib.h>
#include <stdio.h>

# define BLOCK_SIZE		32

# define NB_BLOCKS_HEIGHT	16
# define NB_BLOCKS_WIDTH	32

# define WIN_HEIGHT		NB_BLOCKS_HEIGHT * BLOCK_SIZE
# define WIN_WIDTH		NB_BLOCKS_WIDTH * BLOCK_SIZE

# define SUCCESS	 0
# define FAILURE	-1

typedef enum
  {
    TYPE_EMPTY,
    TYPE_WALL,
    TYPE_LADDER,
    TYPE_KEY,
    TYPE_MONSTER,
    TYPE_ENTRY,
    TYPE_EXIT,
    TYPE_LAST
  }	e_type;

typedef enum
  {
    SPRITE_WALL,
    SPRITE_SCALE,
    SPRITE_KEY,
    SPRITE_MONSTER,
    SPRITE_HERO,
    SPRITE_LANDSCAPE,
    SPRITE_GROUND,
    SPRITE_UPSTAIR,
    SPRITE_BORDER,
    SPRITE_EXIT,
    SPRITE_LAST
  }	e_sprite;


/*
** check.c
*/
void		check_in_out(ssize_t, char*, size_t, FILE*);
void		check_wall_under_monster(char*, int);
int		check_monster(char*);
void		check_wall(char*);

/*
** event.c
*/
void		sdl_init();
void		sdl_event();

/*
** get_map.c
*/
void		map_is_valid(ssize_t read, char *line, size_t len, FILE *fp);
void		check_map(char*);
size_t		get_map_size(char*);
char		*get_map(char*);

/*
** main.c
*/
int		create_screen(SDL_Surface **screen);

/*
** get_max.c
*/
int		get_max_x();
int		get_max_y();


/*
** my_exception.c
*/
void		my_exception(char*);

/*
** sprites.c
*/
SDL_Surface	**load_sprites();

#endif /* !EPIKONG_H_ */
