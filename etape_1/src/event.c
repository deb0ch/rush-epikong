/*
** event.c for epikong in /home/law-du_c/rush-epikong/src/part1
**
** Made by cedric law-dune
** Login   <law-du_c@epitech.net>
**
** Started on  Sat Mar  8 14:00:02 2014 cedric law-dune
** Last update Sat Mar  8 17:25:31 2014 cedric law-dune
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "../include/epikong.h"

void	sdl_init()
{
  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
  SDL_Init(SDL_INIT_VIDEO);
}

void	sdl_event()
{
  SDL_Event event;
  int		i;

  i = 1;
  while (i)
    {
      SDL_WaitEvent(&event);
      if(event.type == SDL_QUIT)
	{
	  i = 0;
	  break;
	}
    }
}
