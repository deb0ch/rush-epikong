/*
** sprites.c for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 18:22:51 2014 Thomas de Beauchene
** Last update Sun Mar  9 18:22:51 2014 cedric law-dune
*/

#include "../include/epikong.h"

SDL_Surface	**load_sprites()
{
  SDL_Surface **sprites;
  int	i;

  if ((sprites = malloc(SPRITE_LAST * sizeof(SDL_Surface*))) == NULL)
    my_exception("could not allocate sprites array");
  sprites[SPRITE_WALL] = IMG_Load("../sprites/world/plateforme_mid.png");
  sprites[SPRITE_GROUND] = IMG_Load("../sprites/world/ground.png");
  sprites[SPRITE_SCALE] = IMG_Load("../sprites/world/scale.png");
  sprites[SPRITE_KEY] = IMG_Load("../sprites/key/key0.png");
  sprites[SPRITE_MONSTER] = IMG_Load("../sprites/monster/move_right2.png");
  sprites[SPRITE_HERO] = IMG_Load("../sprites/rayman/move_left.png");
  sprites[SPRITE_LANDSCAPE] = IMG_Load("../sprites/landscape.bmp");
  sprites[SPRITE_UPSTAIR] = IMG_Load("../sprites/world/wallscale.png");
  sprites[SPRITE_BORDER] = IMG_Load("../sprites/world/plateforme_left.png");
  i = 0;
  while (i < SPRITE_LAST - 1)
    {
      if (sprites[i] == NULL)
	my_exception("could not load sprites");
      i += 1;
    }
  return (sprites);
}
