/*
** check.c for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 21:14:53 2014 Thomas de Beauchene
** Last update Sat Mar  8 21:14:57 2014 Thomas de Beauchene
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../include/epikong.h"

void		check_in_out(ssize_t read, char *line, size_t len, FILE *fp)
{
  int		i;
  int		flag_in;
  int		flag_out;
  int		flag_key;

  flag_in = -1;
  flag_out = -1;
  flag_key = -1;
  while ((read = getline(&line, &len, fp)) != -1)
    {
      i = 0;
      while (line[i])
	{
	  if (line[i] == 'i')
	    flag_in = 1;
	  if (line[i] == 'o')
	    flag_out = 1;
	  if (line[i] == 'k')
	    flag_key = 1;
	  i++;
	}
    }
  if (flag_in != 1 || flag_out != 1 || flag_key != 1)
    my_exception("Invalid map : map need a door, and exit and a key !");
}

void		check_wall_under_monster(char *line, int pos)
{
  if (line[pos] != 'w')
    my_exception("Invalid map : monster is floating in the air !");
}

int		check_monster(char *line)
{
  size_t	i;

  i = 0;
  while (i != (strlen(line) - 1))
    {
      if (line[i] == 'm')
	return (i);
      i++;
    }
  return (-1);
}

void		check_wall(char *line)
{
  size_t	i;

  i = 0;
  while (i != (strlen(line) - 1))
    {
      if (line[i] != 'w')
	my_exception("Invalid map: must be surrounded by walls");
      i++;
    }
}
