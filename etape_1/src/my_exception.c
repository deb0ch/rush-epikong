/*
** my_exception.c for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 17:00:09 2014 Thomas de Beauchene
** Last update Sat Mar  8 17:13:34 2014 Thomas de Beauchene
*/

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include "../include/epikong.h"

void		my_exception(char *msg)
{
  fprintf(stderr, "Error : %s\n", msg);
  SDL_Quit();
  exit(EXIT_FAILURE);
}
