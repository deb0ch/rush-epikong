/*
** get_max.c for epikong in /home/law-du_c/rush-epikong/etape_1/src
** 
** Made by cedric law-dune
** Login   <law-du_c@epitech.net>
** 
** Started on  Sun Mar  9 18:18:34 2014 cedric law-dune
** Last update Sun Mar  9 18:22:50 2014 cedric law-dune
*/

#include "../include/epikong.h"

int		get_max_x()
{
  SDL_Rect	pos;
  int		i;
  int		x_max;
 
  i = 0;
  x_max = 0;
  while (i < NB_BLOCKS_WIDTH * NB_BLOCKS_HEIGHT * BLOCK_SIZE)
    {
      pos.x = (i % NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      if (x_max < pos.x)
	x_max = pos.x;
      ++i;
    }
  return (x_max);
}

int		get_max_y()
{
  SDL_Rect	pos;
  int		i;
  int		y_max;
 
  i = 0;
  y_max = 0;
  while (i < NB_BLOCKS_WIDTH * NB_BLOCKS_HEIGHT * BLOCK_SIZE)
    {
      pos.y = (i / NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      if (y_max < pos.y)
	y_max = pos.y;
      ++i;
    }
  return (y_max);
}
