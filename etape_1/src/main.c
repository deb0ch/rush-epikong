/*
** main.c for epikong in /home/chauvo_t/rendu/rush-epikong/part1
**
** Made by Thomas de Beauchene
** Login   <chauvo_t@epitech.net>
**
** Started on  Sat Mar  8 16:52:49 2014 Thomas de Beauchene
** Last update Sun Mar  9 18:52:31 2014 cedric law-dune
*/

#include "../include/epikong.h"

int		create_screen(SDL_Surface **screen)
{
  *screen = SDL_SetVideoMode(WIN_WIDTH, WIN_HEIGHT, 32, SDL_HWSURFACE);
  SDL_WM_SetCaption("EpiKong", NULL);
  return (EXIT_SUCCESS);
}

void		blit_level(SDL_Surface *screen,
			   char *map,
			   SDL_Surface **sprites)
{
  SDL_Rect	pos;
  int		i;
  int		x_max;
  int		y_max;
  
  y_max = get_max_y();
  x_max = get_max_x();
  i = 0;
  while (i < NB_BLOCKS_WIDTH * NB_BLOCKS_HEIGHT * BLOCK_SIZE)
    {
      pos.x = (i % NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      pos.y = (i / NB_BLOCKS_WIDTH) * BLOCK_SIZE;
      SDL_BlitSurface(sprites[SPRITE_LANDSCAPE], NULL, screen, &pos);
      if (map[i] == 'w')
	SDL_BlitSurface(sprites[SPRITE_WALL], NULL, screen, &pos);
      else if (map[i] == 's')
	{
	  SDL_BlitSurface(sprites[SPRITE_SCALE], NULL, screen, &pos);
	  if (map[i - 1] == 'w' && map[i + 1] == 'w')
	    SDL_BlitSurface(sprites[SPRITE_UPSTAIR], NULL, screen, &pos);
	}
      else if (map[i] == 'o')
	SDL_BlitSurface(sprites[SPRITE_EXIT], NULL, screen, &pos);
      else if (map[i] == 'k')
	SDL_BlitSurface(sprites[SPRITE_KEY], NULL, screen, &pos);
      else if (map[i] == 'm')
	SDL_BlitSurface(sprites[SPRITE_MONSTER], NULL, screen, &pos);
      else if (map[i] == 'i')
	SDL_BlitSurface(sprites[SPRITE_HERO], NULL, screen, &pos);
      if (pos.y == 0 || pos.x == 0 || pos.x == x_max || pos.y == y_max)
	SDL_BlitSurface(sprites[SPRITE_GROUND], NULL, screen, &pos);
      i += 1;
    }
}

int		main(int ac, char **av)
{
  SDL_Surface	*screen;
  SDL_Surface	**sprites;
  char		*map;

  if (ac == 2)
    map = get_map(av[1]);
  else
    {
      printf("USAGE: 1st arg must be the map file.");
      return (EXIT_FAILURE);
    }
  sprites = load_sprites();
  sdl_init();
  create_screen(&screen);
  blit_level(screen, map, sprites);
  SDL_Flip(screen);
  sdl_event();
  SDL_Quit();
  return (EXIT_SUCCESS);
}
